package tdd.training.mra;

import static org.junit.Assert.*;
import org.junit.Test;

import java.awt.List;
import java.util.*;;

public class MarsRoverTest {

	@Test
	public void testMarsRoverWithoutObstacles() throws MarsRoverException {
		int [][] testMRover = new int[10][12];
		MarsRover rover = new MarsRover(10, 12, null);
		Arrays.deepEquals(testMRover, rover.marsRover);
	}

	@Test
	public void testMarsRoverWithObstacles() throws MarsRoverException {
		int [][] testMRover = new int[10][12];
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		Arrays.deepEquals(testMRover, rover.marsRover);
	}
	
	@Test
	public void testPlanetContainsObstaclesAt() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals(true, rover.planetContainsObstacleAt(5, 5));
	}

	@Test
	public void testExecuteCommandWhenItReceivesAnEmptyCommandString() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testTurning() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testMovingForward() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals("(0,1,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testMovingBackward() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.executeCommand("f");
		assertEquals("(0,0,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void testMovingCombined() throws MarsRoverException {
		ArrayList<String> planetObstacles = new ArrayList<String>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
}
