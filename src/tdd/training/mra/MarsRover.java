package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	public int[][] marsRover;
	public ArrayList<String> obstacles;
	public String status;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		int x = planetX;
		int y = planetY;
		
		marsRover = new int[x][y];
		if (planetObstacles != null) {
			obstacles = new ArrayList<>(planetObstacles);
		}
		status = "(0,0,N)";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if ((x > marsRover[0].length) || (y > marsRover[1].length) || (x < 0) || (y < 0)) {
			throw new MarsRoverException("Coordinate errate");
		}
		
		int xC = -1;
		int yC = -1;
		for (int i = 0; i < obstacles.size(); i++) {
			xC = Character.getNumericValue(obstacles.get(i).charAt(1));
			yC = Character.getNumericValue(obstacles.get(i).charAt(3));
			if ((xC == x) && (yC == y)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {	
		String[] comandi;
		comandi = new String[commandString.length()];
		
		if(commandString == "") {
			return status;
		}
		
		if (commandString == "r") {
			status = status.substring(0, 5) + "E)";
		}
		
		if (commandString == "l") {
			status = status.substring(0, 5) + "W)";
		}
		
		if (commandString == "f") {
			if(status.charAt(5) == 'N') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(3)) + 1;
				status = status.substring(0, 3) + nuovaCoordinataDopoLoSpostamento + ",N)";
			}
			
			if(status.charAt(5) == 'S') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(3)) - 1;
				status = status.substring(0, 3) + nuovaCoordinataDopoLoSpostamento + ",N)";
				return status;
			}
			
			if(status.charAt(5) == 'E') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(1)) + 1;
				status = "(" + nuovaCoordinataDopoLoSpostamento + status.substring(2, status.length() - 1);
				return status;
			}
			
			if(status.charAt(5) == 'W') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(1)) - 1;
				status = "(" + nuovaCoordinataDopoLoSpostamento + status.substring(2, status.length() - 1);
			}	
		}
		
		if (commandString == "b") {
			if(status.charAt(5) == 'N') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(3)) - 1;
				status = status.substring(0, 3) + nuovaCoordinataDopoLoSpostamento + ",N)";
			}
			
			if(status.charAt(5) == 'S') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(3)) + 1;
				status = status.substring(0, 3) + nuovaCoordinataDopoLoSpostamento + ",N)";
			}
			
			if(status.charAt(5) == 'E') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(1)) - 1;
				status = "(" + nuovaCoordinataDopoLoSpostamento + status.substring(2, status.length() - 1);
			}
			
			if(status.charAt(5) == 'W') {
				int nuovaCoordinataDopoLoSpostamento = Character.getNumericValue(status.charAt(1)) + 1;
				status = "(" + nuovaCoordinataDopoLoSpostamento + status.substring(2, status.length() - 1);
			}	
		}
		
		for (int i = 0; i < (commandString.length() - 1); i++) {
			comandi[i] = String.valueOf(commandString.charAt(i));
			executeCommand(comandi[i]);
		}
		
		return status;
	}

}
